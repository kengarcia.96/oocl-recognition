import * as types from '../../actionTypes'

const initialState = {
    achievements: [],
    awards : []
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_ACHIEVEMENTS: {
            return {
                ...state,
                achievements: action.payload
            }
        }
        case types.FETCH_ALL_AWARDS : {
            return {
                ...state,
                awards : action.payload
            }
        }
        default:
            return state
    }
}