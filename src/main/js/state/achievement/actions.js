import axios from 'axios'
import * as types from '../../actionTypes'


const url = '/api/achievements'

export const fetchAllAchievements = () => {
    return async (dispatch) => {
        const response = await axios.get(url)
        dispatch({type: types.FETCH_ACHIEVEMENTS, payload: response.data})
    }
}

export const fetchAllAwards = () => {
    return async (dispatch) => {

        dispatch({type: types.SHOW_LOADING})
        const response = await axios.get('/api/awards/getAll')

        dispatch({type: types.FETCH_ALL_AWARDS, payload: response.data})
        dispatch({type: types.HIDE_LOADING})
    }
}