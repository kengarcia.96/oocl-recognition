import * as types from '../../actionTypes'

const initialState = {
    requests: [],
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_REQUESTS: {
            return {
                ...state,
                requests: action.payload
            }
        }
        default:
            return state
    }
}