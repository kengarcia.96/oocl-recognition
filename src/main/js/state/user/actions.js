import axios from 'axios'
import * as types from '../../actionTypes'


const url = '/api/users'

export const fetchAllUserDropDown = () => {
    return async (dispatch) => {
        const response = await axios.get(url + '/getAll')
        const dropDownUsers = []

        response.data.forEach((user)=>{
            let dropdown ={
                key: user.username,
                text: user.fullName,
                value: user.id,
                image: {avatar: true, src: '/images/avatar/'+ user.avatar}
            }

            dropDownUsers.push(dropdown)
        })

        dropDownUsers.sort((a, b) => a.text.localeCompare(b.text))
        dispatch({type: types.FETCH_ALL_USER_DROPDOWN, payload: dropDownUsers})
    }
}