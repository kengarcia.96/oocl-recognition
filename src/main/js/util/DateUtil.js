export const deriveDateToday = (requestDate) => {
    let today = new Date();
    if(requestDate) {
        today = requestDate
    }

    let dd = today.getDate();

    let mm = today.getMonth()+1;
    const yyyy = today.getFullYear();
    if(dd < 10) {
        dd = '0' + dd;
    }

    if(mm < 10) {
        mm = '0' + mm;
    }
    return yyyy+''+mm+''+dd
}

export const formatDate = (date) => {
    if (!date) {
        return ''
    }

    const year = date.slice(0, 4)
    const month = date.slice(4, 6)
    const day = date.slice(6, 8)
    return `${month}/${day}/${year}`
}