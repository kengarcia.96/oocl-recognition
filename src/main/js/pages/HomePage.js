import React, {Component} from 'react'
import {Button, Card, CardContent, Grid, GridColumn, GridRow, Header} from "semantic-ui-react"
import VoyageCard from "../components/VoyageCard"
import Table from "../components/Table"
import {closeRejectModal, fetchAllPendingApproval, openRejectModal} from "../state/pendingApproval/actions"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

export class HomePage extends Component {
    constructor(props){
        super(props)
        this.state = {
            voyageCards: [{
                name: "John Doe",
                team: "GSP Team",
                description: "1st Voyage",
                imageSrc: '/images/avatar/johnDoe.png',
                dateAwarded: 'June 14'
            }, {
                name: "Jane Doe",
                team: "Booking Team",
                description: "3rd Voyage",
                imageSrc: '/images/avatar/janeDoe.png',
                dateAwarded: 'June 14'
            }, {
                name: "Jane Doe",
                team: "Booking Team",
                description: "3rd Voyage",
                imageSrc: '/images/avatar/janeDoe.png',
                dateAwarded: 'June 14'
            }, {
                name: "Jane Doe",
                team: "Booking Team",
                description: "3rd Voyage",
                imageSrc: '/images/avatar/janeDoe.png',
                dateAwarded: 'June 14'
            }],
            pendingApprovals: [{
                id: 0,
                awardeeName: 'John Doe',
                awardeeTeam: 'GSP',
                awardeeDepartment: 'ISD',
                requestorName: 'Jonathan Doe',
                requestorTeam: 'ITS',
                requestorDepartment: 'ISD',
                isSupervisor: 'Yes',
                containerCount: 2,
                requestDate: '2019-01-05',
                attributes: 'Happy, Positive, Share',
                remarks: 'John Doe helped us do something something something something'
            }, {
                id: 1,
                awardeeName: 'Junatan Cooper',
                awardeeTeam: 'BKG',
                awardeeDepartment: 'ISD',
                requestorName: 'Juswa Asylum',
                requestorTeam: 'SHP',
                requestorDepartment: 'ISD',
                isSupervisor: 'No',
                containerCount: 2,
                requestDate: '2019-01-05',
                attributes: 'Innovation',
                remarks: 'Junatan helped us do something something something something'
            }, {
                id: 2,
                awardeeName: 'Dikster Pavilion',
                awardeeTeam: 'BKG',
                awardeeDepartment: 'ISD',
                requestorName: 'Brad Pete',
                requestorTeam: 'SHP',
                requestorDepartment: 'ISD',
                isSupervisor: 'No',
                containerCount: 2,
                requestDate: '2019-01-05',
                attributes: 'Innovation, Share, Happy',
                remarks: 'Dikster helped us do something something something something'
            }, {
                id: 3,
                awardeeName: 'Dikster Pavilion',
                awardeeTeam: 'BKG',
                awardeeDepartment: 'ISD',
                requestorName: 'Brad Pete',
                requestorTeam: 'SHP',
                requestorDepartment: 'ISD',
                isSupervisor: 'No',
                containerCount: 2,
                requestDate: '2019-01-05',
                attributes: 'Innovation, Share, Happy',
                remarks: 'Dikster helped us do something something something something'
            }],
            columns: [{
                field: 'awardee',
                label: 'Awardee',
                renderer: (data) => (
                    <Header as='h4' image>
                        <Header.Content>
                            {data.awardeeName}
                            <Header.Subheader>Requestor : {data.requestorName}</Header.Subheader>
                        </Header.Content>
                    </Header>
                )
            }, {
                field: 'remarks',
                label: 'Remarks'
            }, {
                field: 'action',
                label: 'Action',
                renderer: () => (
                    <div style={{display:'grid', width:150}}>
                        <Button id='approve-btn' size='tiny' style={{backgroundColor: '#EB5757', color: 'white', margin:2.5, fontSize:12, fontWeight: 900}}
                                onClick={this.handleOnApprove.bind(this)}>Approve</Button>
                        <Button id='reject-btn' size='tiny' style={{backgroundColor: '#BDBDBD', color: 'white', margin:2.5}}
                                onClick={this.handleOnReject.bind(this)}>Reject </Button>
                    </div>
                )
            }]
        }
    }

    handleOnApprove(){
        console.log('Approve');
    }

    handleOnReject(){
        console.log('clicked')
        this.props.actions.openRejectModal()
    }


    render() {
        let cardStyle = {
            display: 'flex',
            height: 700,
            width:'100%',
            border: '1px solid #E0E0E0',
            boxSizing: 'border-box',
            borderRadius: 5,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            overflow: 'auto',
            flexDirection: 'row'
        }
        return (
            <Grid padded columns='equal' style={{height:800}}>
                <GridRow stretched>
                    <GridColumn style={{display:'flex'}}>
                        <div id="for-approval" style={{height:'50%',paddingLeft:25}}>
                            <Header as='h1'>For My Approval</Header>
                            <Card fluid>
                                <CardContent style={{
                                    backgroundColor: '#FFFFFF', height: 320, flexWrap: 'wrap',
                                    justifyContent: 'space-between',
                                    overflow: 'auto',
                                    flexDirection: 'row'
                                }}>
                                    <Table datas={this.state.pendingApprovals} columns={this.state.columns} />
                                </CardContent>
                            </Card>
                        </div>
                        <div id="pending-request" style={{height:'50%', paddingLeft:25}}>
                            <Header as='h1'>My Pending Request</Header>
                            <Card fluid>
                                <CardContent  style={{
                                    backgroundColor: '#FFFFFF', height: 320, flexWrap: 'wrap',
                                    justifyContent: 'space-between',
                                    overflow: 'auto',
                                    flexDirection: 'row'
                                }}>
                                    <Table datas={this.state.pendingApprovals} columns={this.state.columns} />
                                </CardContent>
                            </Card>
                        </div>
                    </GridColumn>
                    <GridColumn stretched>
                        <div id="recently-awarded" style={{height:'auto'}}>
                            <Header as='h1'>Recently Awarded</Header>
                            <Card style={cardStyle}>
                                {this.getRecentlyAwardedCards()}
                            </Card>
                        </div>
                    </GridColumn>
                </GridRow>
            </Grid>
        )
    }

    getRecentlyAwardedCards() {
        return this.state.voyageCards.map((card, i) => (
            <div key={i} style={{width:'300', height:'auto', margin:'20px 50px'}}>
                <VoyageCard
                    data={{name: card.name, team: card.team, description: card.description +" / "+card.dateAwarded, imageSrc: card.imageSrc}}/>
            </div>))
    }
}


function mapStateToProps(state) {
    return {
        pendingApprovals: state.pendingApproval.pendingApprovals,
        rejectModalOpen: state.pendingApproval.rejectModalOpen
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({fetchAllPendingApproval, closeRejectModal, openRejectModal}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)