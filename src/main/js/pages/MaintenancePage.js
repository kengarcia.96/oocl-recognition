import React, {Component} from 'react'
import {fetchAllUser} from "../state/maintenance/actions"
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

export class MaintenancePage extends Component {
    render() {
        return (
            <div>
                MAINTENANCE PAGE
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        users: state.maintenance.users
    }
}

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators({fetchAllUser}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(MaintenancePage)