import React, {Component} from 'react'

class Slider extends Component {


    render() {
        let {children} = this.props
        return (
            <div className="cards-slider">
                <div className="slider-buttons">
                    <button className="slider-button button-left" onClick={() => this.handleClick('prev')}>&lt;</button>
                    <button className="slider-button button-right" onClick={() => this.handleClick('next')}>&gt;</button>
                </div>
                {children}
            </div>
        )
    }
}

export default Slider