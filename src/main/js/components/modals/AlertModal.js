import React, {Component} from 'react'
import {Button, Modal} from "semantic-ui-react"
import * as statics from "../../util/statics"

class AlertModal extends Component {
  getCancelButton(action, icon, position, content) {
      return <Button secondary icon={icon} labelPosition={position} content={content}
                          onClick={action}/>
  }

  getOkayButton(action, icon, position, content) {
      if(this.props.pageType === statics.PENDING_APPROVAL_PAGE) {
          return (
              <Button.Group>
                  <Button positive animated='vertical' onClick={action}>
                    <Button.Content visible content={'1 Point'}/>
                    <Button.Content hidden content={'Okay'}/>
                  </Button>
                  <Button.Or/>
                  <Button positive animated='vertical' onClick={action}>
                    <Button.Content visible content={'2 Point'}/>
                    <Button.Content hidden content={'Okay'}/>
                  </Button>
              </Button.Group>
          )
      }

      return <Button positive icon={icon} labelPosition={position} content={content}
                          onClick={action}/>
  }

  render() {
      const modalStyle = {
          width: 500,
          height: 'auto',
          padding: 5,
          top: 'calc(50vh - 100px)',
          left: 'calc(50vw - 250px)'
      }
      const {onClose, onConfirm} = this.props
      return (
          <Modal size={"tiny"} open={this.props.open} onClose={this.props.onClose} style={modalStyle}>
              <Modal.Header>{this.props.title}</Modal.Header>
              <Modal.Content>
                  <p>{this.props.message}</p>
              </Modal.Content>
              <Modal.Actions>
                  {onClose && this.getCancelButton(onClose, 'cancel', 'right', 'Cancel')}
                  {onConfirm && this.getOkayButton(onConfirm, 'checkmark', 'right', 'Okay')}
              </Modal.Actions>
          </Modal>
      )
  }
}

export default AlertModal