import React, {Component} from 'react'
import {Button, Modal, TextArea} from "semantic-ui-react"
import {bindActionCreators} from "redux"
import {rejectAward, fetchAllPendingApproval} from "../../state/pendingApproval/actions"
import {connect} from "react-redux"
import AlertModal from "./AlertModal"

export class RejectModal extends Component {
  constructor(props){
    super(props)
    this.state = {
      rejectionRemarks: '',
      showAlertModal: false
    }
  }
  handleRemarksChange(event){
    this.setState({rejectionRemarks: event.target.value})
  }

  handleOnAlertModalSubmit() {
    this.setState({showAlertModal: false})
  }

  onClickOkReject(){
    this.props.actions.rejectAward(this.props.awardId, this.state.rejectionRemarks).then(()=>{
      this.setState({rejectionRemarks:'', showAlertModal: true})
      this.props.actions.fetchAllPendingApproval()
      this.props.closeAction()
    })
  }

  render() {
        const modalStyle = {
            width: 500,
            height: 'auto',
            padding: 5,
            top: 'calc(50vh - 100px)',
            left: 'calc(50vw - 250px)'
        }
        const buttonStyle = {
            margin: '10px 5px', color: 'white', width: 120
        }
        const {closeAction} = this.props

        let handleCloseAction = closeAction === undefined ? null : closeAction

        return (
            <div>
              <AlertModal open={this.state.showAlertModal}
                                  onConfirm={this.handleOnAlertModalSubmit.bind(this)}
                                  message='Award rejected!'
                                  title='Notification'/>
              <Modal size={'tiny'} open={this.props.open} onClose={this.props.onClose} style={modalStyle}
                     dimmer='blurring'>
                  <Modal.Content style={{alignItems: 'stretch', display: 'flex', flexDirection: 'column', padding: 10}}>
                      <TextArea style={{height: 200, padding: 8, fontStyle: 'italic', resize: 'none'}}
                                placeholder="Please provide details on why the request was rejected..."
                                value = {this.state.rejectionRemarks}
                                onChange = {this.handleRemarksChange.bind(this)}
                      />
                  </Modal.Content>
                  <div style={{display: 'flex', justifyContent: 'center'}}>
                      <Button id='approve-btn' size='tiny'
                              style={{...buttonStyle, backgroundColor: '#EB5757'}}
                              onClick={this.onClickOkReject.bind(this)}>OK</Button>
                      <Button id='reject-btn' size='tiny'
                              style={{...buttonStyle, backgroundColor: '#BDBDBD'}}
                              onClick={handleCloseAction}>Cancel</Button>
                  </div>
              </Modal>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators({rejectAward, fetchAllPendingApproval}, dispatch)}
}

export default connect(null, mapDispatchToProps)(RejectModal)
