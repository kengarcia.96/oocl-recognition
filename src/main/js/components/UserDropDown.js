import React, {Component} from 'react'
import {Dropdown} from "semantic-ui-react"

class UserDropDown extends Component {
  render() {
    return (
        <div style={this.props.style}>
          <Dropdown
              className={'user-list'}
              fluid
              search
              selection
              clearable
              options={this.props.options}
              value={this.props.value}
              onChange={this.props.onChange}
          />
        </div>
    )
  }
}

export default UserDropDown