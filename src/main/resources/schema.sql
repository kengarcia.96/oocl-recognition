create or replace table if not exists award
(
	ID int auto_increment
		primary key,
	AWARDEE_USER_ID int not null,
	REQUESTOR_USER_ID int not null,
	REQUESTOR_TEAM varchar(50) default '' not null,
	IS_SUPERVISOR tinyint(1) null,
	AWARDEE_TEAM varchar(50) default '' not null,
	ATTRIBUTES varchar(250) null,
	REMARKS varchar(250) null,
	APPROVAL_STATUS varchar(20) null,
	APPROVAL_DATE varchar(20) null,
	REJECTION_REMARKS varchar(250) null,
	POINTS int(4) null,
	AWARDEE_CLUSTER varchar(50) null,
	REQUESTOR_CLUSTER varchar(50) null,
	REQUESTED_DATE varchar(25) null
);

create or replace table if not exists milestone
(
	ID int auto_increment
		primary key,
	USER_ID int not null,
	CREATED_DATE date null,
	VOYAGE int null,
	POINTS int null
);

create or replace table if not exists user
(
	ID int auto_increment
		primary key,
	FIRSTNAME varchar(50) null,
	USERNAME varchar(16) default '' not null,
	PASSWORD varchar(200) default '' not null,
	ROLE varchar(50) default '' not null,
	MIDDLENAME varchar(50) null,
	LASTNAME varchar(50) null,
	EMAIL varchar(100) null,
	TEAM varchar(50) null,
	CLUSTER varchar(50) null
);