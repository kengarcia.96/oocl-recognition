package com.community.recognition.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MessageUtil {

    private String name;

    public String getMessage(String action) {
        if("APPROVED".equals(action)) {
          return "Congratulation " + this.name + " for receiving a container award";
        }else if("MILESTONE_ACHIEVED".equals(action)) {
          return "Congratulation " + this.name + " for completing another voyage. Keep it up!";
        }else if("PENDING".equals(action)) {
            return "A request has been submitted for " + this.name;
        }else{
          return "";
        }
    }
}