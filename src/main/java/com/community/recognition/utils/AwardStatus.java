package com.community.recognition.utils;

public class AwardStatus {

  public static final String APPROVED = "APPROVED";
  public static final String MILESTONE_ACHIEVED = "MILESTONE_ACHIEVED";
  public static final String UPDATED = "UPDATED";
  public static final String PENDING = "PENDING";
  public static final String REJECTED = "REJECTED";
}