package com.community.recognition.integration;

import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.Test;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

public class AchievementLayerTest extends BaseIntegrationTest {

  /**
   * No need to continue integration test for milestone layer for now
   * as the current JS unit test should cover the testing for this layer
   */
  @Test
  public void testAchievementLayer() {
    goToPage("Achievement");

    FluentWebElement achievementsTable = $(".ui.celled.collapsing.very.basic.table").first();
    FluentWebElement voyageCardImages = $("img[src='/images/avatar/alien.jpeg']").first();

    assertThat(achievementsTable).isDisplayed();
    assertThat(voyageCardImages).isDisplayed();
  }
}
