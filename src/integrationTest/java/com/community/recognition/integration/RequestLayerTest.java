package com.community.recognition.integration;

import static org.fluentlenium.assertj.FluentLeniumAssertions.assertThat;

import java.util.HashMap;
import org.fluentlenium.core.domain.FluentWebElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class RequestLayerTest extends BaseIntegrationTest {
    @Autowired
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {
        //delete created request so it will not affect other test class
       namedParameterJdbcTemplate.update("DELETE FROM award WHERE ID = '5'", new HashMap<>());
    }

    @Test
    public void testRequestLayer() {
        testSaveRequest();
        testResetRequest();
    }

    private void testResetRequest() {
        goToPage("Request");

        FluentWebElement page = $(".stretched.fourteen.wide.column").first();
        inputInfo(page, "#awardee-details", "Gene Anthony Kadano", "SHP", "SHIPMENT SERVICES");
        inputInfo(page, "#requestor-details", "Gene Anthony Kadano", "SHP", "SHIPMENT SERVICES");

        selectAttribute(page, "thoroughness");
        selectAttribute(page, "accuracy");

        page.$("textarea[name='remarks']").first().fill().withText("This is a remarks");
        assertThat(page.$("textarea[name='remarks']").first().textContent()).isEqualTo("This is a remarks");

        //Reset button
        page.$(".ui.massive.button").last().click();

        assertThat(page.$(".awardee-attributes input[name='thoroughness']").first()).isNotSelected();
        assertThat(page.$(".awardee-attributes input[name='accuracy']").first()).isNotSelected();
        assertThat(page.$("textarea[name='remarks']").first().textContent()).isEqualTo("");
    }

    private void testSaveRequest() {
        goToPage("Request");

        FluentWebElement page = $(".stretched.fourteen.wide.column").first();
        inputInfo(page, "#awardee-details", "Aljun Dequilla", "SHP", "SHIPMENT SERVICES");
        inputInfo(page, "#requestor-details", "Aljun Dequilla", "SHP", "SHIPMENT SERVICES");

        selectAttribute(page, "ropv");
        selectAttribute(page, "edo");

        page.$("textarea[name='remarks']").first().fill().withText("This is a remarks");
        assertThat(page.$("textarea[name='remarks']").first().textContent()).isEqualTo("This is a remarks");

        //Submit button
        page.$(".ui.massive.button").first().click();

        waitUntil(".ui.tiny.modal.transition.visible.active");

        FluentWebElement modal = $(".ui.tiny.modal.transition.visible.active").first();
        assertThat(modal.$(".header").first().textContent()).isEqualTo("Request Submitted");
        assertThat(modal.$(".content").first().textContent()).isEqualTo("Request is successfully submitted for approval");
        assertThat(modal.$(".actions .ui.icon.positive.right.labeled.button").first().textContent()).isEqualTo("Okay");

        clickOn(modal.$(".actions .ui.icon.positive.right.labeled.button").first());
        assertThat(page.html()).doesNotContain("ui tiny modal transition visible active");
    }

    private void inputInfo(FluentWebElement page, String details, String name, String team, String cluster) {
        String teamName = "#awardee-details".equals(details) ? "awardeeTeam" : "requestorTeam";

        page.$(details + " .content .user-list .search").first().fill().withText(name);
        page.$(details + " .content .user-list div[role='listbox'] div[role='option']").first().click();
        page.$(details + " .content input[name='" + teamName + "']").first().fill().withText(team);
        page.$(details + " .content .cluster-list .search").first().fill().withText(cluster);
        page.$(details + " .content .cluster-list div[role='listbox'] div[role='option']").first().click();
    }

    private void selectAttribute(FluentWebElement page, String attributeCode) {
        page.$(".awardee-attributes input[name='" + attributeCode + "']~label").click();
        assertThat(page.$(".awardee-attributes input[name='" + attributeCode + "']").first()).isSelected();
    }
}
