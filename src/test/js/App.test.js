import {shallow} from 'enzyme'
import React from "react"

import {App} from "../../main/js/App"
import NavBar from "../../main/js/components/NavBar"
import {Route} from "react-router-dom"
import MaintenancePage from "../../main/js/pages/MaintenancePage"
import AchievementPage from "../../main/js/pages/AchievementPage"
import PendingApprovalPage from "../../main/js/pages/PendingApprovalPage"
import RequestPage from "../../main/js/pages/RequestPage"
import HomePage from "../../main/js/pages/HomePage"
import {LoginPage} from "../../main/js/pages/LoginPage"
import * as statics from "../../main/js/util/statics"
import * as milestoneActions from "../../main/js/state/milestone/actions"
import * as pendingApprovalActions from "../../main/js/state/pendingApproval/actions"

describe('App Test ', () => {
    let props, wrapper, wrapperInstance, fetchAllMilestonesSpy, fetchAllPendingApprovalSpy

    beforeEach(() => {
        window.alert = jest.fn()
        fetchAllMilestonesSpy = jest.spyOn(milestoneActions, 'fetchAllMilestones')
        fetchAllPendingApprovalSpy = jest.spyOn(pendingApprovalActions, 'fetchAllPendingApproval')

        props = {
            location: {
                pathname : '/home'
            } ,
            appInitialized: false,
            actions: {
                initializeApp: jest.fn(),
                fetchAllMilestones: fetchAllMilestonesSpy,
                fetchAllPendingApproval: fetchAllPendingApprovalSpy
            },
        }
        wrapper = shallow(<App {...props}/>)
        wrapperInstance = wrapper.instance()
    })

    it('renders the component route NOT login', () => {
        expect(wrapper).toHaveComponent(NavBar)
    })

    it('contains routes to each page when not logged in', () => {
        const routes = wrapper.find(Route)
        const pathMap = {}
        routes.forEach(route => {
            pathMap[route.props().path] = route.props().component ? route.props().component : route.props().children
        })

        expect(pathMap["/"]).toBeTruthy()
        expect(pathMap["/home"]).toBe(HomePage)
        expect(pathMap["/maintenance"]).toBe(MaintenancePage)
        expect(pathMap["/request"]).toBe(RequestPage)
        expect(pathMap["/achievement"]).toBe(AchievementPage)
        expect(Object.keys(pathMap).length).toEqual(5)
    })

    it('contains routes to each page when logged in', () => {
        props.userProfile = {
            authorities: [{
                authority: 'APPROVER'
            }]
        }

        wrapper = shallow(<App {...props}/>)
        wrapperInstance = wrapper.instance()

        const routes = wrapper.find(Route)
        const pathMap = {}
        routes.forEach(route => {
            pathMap[route.props().path] = route.props().component ? route.props().component : route.props().children
        })

        expect(pathMap["/"]).toBeTruthy()
        expect(pathMap["/home"]).toBe(HomePage)
        expect(pathMap["/maintenance"]).toBe(MaintenancePage)
        expect(pathMap["/request"]).toBe(RequestPage)
        expect(pathMap["/achievement"]).toBe(AchievementPage)
        expect(pathMap["/pendingApproval"]).toBe(PendingApprovalPage)
        expect(Object.keys(pathMap).length).toEqual(6)
    })

    it('should fetch all milestone and pending approval on message receive', () => {
        const notification = {
            action: statics.REQUEST_APPROVED
        }
        wrapperInstance.onMessageReceive(notification)

        expect(fetchAllMilestonesSpy).toHaveBeenCalled()
        expect(fetchAllPendingApprovalSpy).toHaveBeenCalled()
    })
})