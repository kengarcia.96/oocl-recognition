import reducer from "../../../../main/js/state/app/reducers"
import {checkReducerFn, clone} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('app reducers', () => {
    let checkReducer

    const initialState = {
        appInitialized: false,
        loading : false,
        loggedIn: false,
        hasError: false,
        profile: null
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles app initialization', () => {
        const action = {type: types.APP_INITIALIZED}
        const expectedState = clone(initialState)
        expectedState.appInitialized = true
        checkReducer(action, expectedState)
    })

    it('handles show loading', () => {
        const action = {type: types.SHOW_LOADING}
        const expectedState = clone(initialState)
        expectedState.loading = true
        checkReducer(action, expectedState)
    })

    it('handles hide loading', () => {
        const action = {type: types.HIDE_LOADING}
        const expectedState = clone(initialState)
        expectedState.loading = false
        checkReducer(action, expectedState)
    })

    it('handles show error', () => {
        const action = {type: types.HAS_ERROR}
        const expectedState = clone(initialState)
        expectedState.hasError = true
        checkReducer(action, expectedState)
    })

    it('handles hide error', () => {
        const action = {type: types.CLEAR_ERROR}
        const expectedState = clone(initialState)
        expectedState.hasError = false
        checkReducer(action, expectedState)
    })
})