import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/app/actions'
import * as types from '../../../../main/js/actionTypes'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('App actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({app: {hasError: false}})
    })

    it('handles app initialization', () => {
        const expectedActions = [
            {type: types.APP_INITIALIZED}
        ]

        store.dispatch(actions.initializeApp())
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('handles log error', () => {
        const expectedActions = [
            {type: types.HAS_ERROR}
        ]
        store.dispatch(actions.logError())
        expect(store.getActions()).toEqual(expectedActions)
    })

    it('handles clear error', () => {
        const expectedActions = [
            {type: types.CLEAR_ERROR}
        ]
        store.dispatch(actions.clearError())
        expect(store.getActions()).toEqual(expectedActions)
    })
})