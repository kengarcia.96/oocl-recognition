import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/milestone/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from 'axios'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Milestone actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('should fetch all milestone', () => {
        const mockReturnValue = [{
            created_date: '2019-07-25T16:00:00.000+0000',
            id: 1,
            points: 28,
            user: {
                avatar: 'alien.jpeg',
                cluster: 'SHIPMENT SERVICES',
                email: 'flamezi@oocl.com',
                firstname: 'Ziegfreid Morissey',
                fullName: 'Ziegfreid Morissey Flameño',
                id: 1,
                lastname: 'Flameño',
                middlename: null,
                password: '$2a$11$gdMbSMmaAu.bQABO.rbrW.IppGeIsA9KHxGZUCVZTcaVSDAxljT3K',
                role: 'EMPLOYEE',
                team: 'ISD',
                username: 'flamezi'
            },
            voyage: 1
        }]
        const expectedActions = [
            {
                type: types.SHOW_LOADING
            },
            {
                type: types.FETCH_ALL_MILESTONES,
                payload: {
                    voyage: [{
                        description: "1 Voyage",
                        imageSrc: "/images/avatar/alien.jpeg",
                        name: "Ziegfreid Morissey Flameño",
                        team: "ISD"
                    }],
                    table: [{
                        id: 0,
                        imageSrc: "/images/avatar/alien.jpeg",
                        name: "Ziegfreid Morissey Flameño",
                        stars: 28,
                        team: "ISD"
                    }]
                }
            },
            {
                type: types.HIDE_LOADING
            }]
        const expectedURL = '/api/milestone/getAll'
        jest.spyOn(axios, 'get').mockReturnValueOnce(Promise.resolve({data: mockReturnValue}))

        return store.dispatch(actions.fetchAllMilestones()).then(()=>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })
    })
})