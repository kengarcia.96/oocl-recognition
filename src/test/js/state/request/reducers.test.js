import reducer from "../../../../main/js/state/request/reducers"
import {checkReducerFn, clone} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('request reducers', () => {
    let checkReducer

    const initialState = {
        requests: []
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })


    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles fetch request', () => {
        const action = {type: types.FETCH_REQUESTS, payload : [{request : 'Hip hop'},{request: 'Lofi Hip hop'}]}
        const expectedState = clone(initialState)
        expectedState.requests = [{request : 'Hip hop'},{request: 'Lofi Hip hop'}]
        checkReducer(action, expectedState)
    })

})