import reducer from "../../../../main/js/state/achievement/reducers"
import {clone, checkReducerFn} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('achievement reducers', () => {
    let checkReducer

    const initialState = {
        achievements: [],
        awards : []
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles fetch achievements', () => {
        const action = {type: types.FETCH_ACHIEVEMENTS, payload: [{message : 'di ba nga eto ang yong gusto?'},{message: 'ben&ben'}]}
        const expectedState = clone(initialState)
        expectedState.achievements = [{
            message: 'di ba nga eto ang yong gusto?'
        },{
            message: 'ben&ben'
        }]
        checkReducer(action, expectedState)
    })

    it('handles fetch awards', () => {
        const action = {type: types.FETCH_ALL_AWARDS, payload: [{message : 'wish1075'},{message: 'wish exclusive'}]}
        const expectedState = clone(initialState)
        expectedState.awards = [{
            message: 'wish1075'
        },{
            message: 'wish exclusive'
        }]
        checkReducer(action, expectedState)
    })


})