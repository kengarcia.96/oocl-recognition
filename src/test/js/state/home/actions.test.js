import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import * as actions from '../../../../main/js/state/pendingApproval/actions'
import * as types from '../../../../main/js/actionTypes'
import axios from "axios"

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


describe('Pending Approval actions', () => {
    let store
    beforeEach(() => {
        store = mockStore({})
    })

    it('handles fetchAllPendingApproval', () => {
        const expectedActions = [
            {type: types.FETCH_PENDING_APPROVAL}
        ]

        const mockReturnValue = [{
            id : 1,
            name : 'John Cena',
        }]
        const expectedURL = '/api/awards/getAllPendingApprovals'
        jest.spyOn(axios, "get").mockReturnValueOnce(Promise.resolve(mockReturnValue))

        return store.dispatch(actions.fetchAllPendingApproval()).then(() =>{
            expect(axios.get).toHaveBeenCalledWith(expectedURL)
            expect(store.getActions()).toEqual(expectedActions)
            axios.get.mockReset()
        })

    })
})