import reducer from "../../../../main/js/state/pendingApproval/reducers"
import {checkReducerFn, clone} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('pendingApproval reducers', () => {
    let checkReducer

    const initialState = {
        pendingApprovals: [],
        rejectModalOpen: false,
        approvedAward: null
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles fetch pending approval', () => {
        const action = {type: types.FETCH_PENDING_APPROVAL, payload : [{request : 'Hip hop'},{request: 'Lofi Hip hop'}]}
        const expectedState = clone(initialState)
        expectedState.pendingApprovals = [{request : 'Hip hop'},{request: 'Lofi Hip hop'}]
        checkReducer(action, expectedState)
    })


})