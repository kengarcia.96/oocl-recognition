export const matchers = {
    toHaveComponent: (rootComponent, toBeFound) => genericExpect(rootComponent, toBeFound, 1),
    toBeIn : (value, constantAttributes) => findConstantAttributeValue(value, constantAttributes )

}

expect.extend(matchers)

const genericExpect = (component, toBeFound, expectedQuantity) => {
    const actualQuantity = component.find(toBeFound).length;
    const pass = actualQuantity === expectedQuantity

    const name = isFunction(toBeFound) ? toBeFound.name : toBeFound

    const message = () => {
        return pass
            ? `Expected to find NO instances of '${name}', but found ${actualQuantity}`
            : `Expected to find ${expectedQuantity} instances of '${name}', but found ${actualQuantity}`
    }

    return {actual: component, message, pass};
}

const isFunction = (functionToCheck) => {
    const getType = {}
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]'
}

const findConstantAttributeValue = (value, constantAttribute) => {
    const pass = constantAttribute.includes(value)
    const message = () => {
        return pass ? 'Can find in the constant attributes' : `Cannot find ${value} in our defined constant attribute`
    }
    return {actual: value, message, pass};
}