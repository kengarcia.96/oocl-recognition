import {shallow} from "enzyme";
import RequestInfoCard from "../../../main/js/components/RequestInfoCard"
import React from "react";
import {Card, TextArea} from "semantic-ui-react"
import {findManyComponents} from "../testHelpers"


describe('RequestInfoCard test', ()=> {
    it('renders with details', ()=>{
        let props = {
            data:{
                awardeeName:"awardee moto",
                awardeeTeam:"team moto",
                awardeeDepartment:"dept moto",
                requestorName:"requestor moto",
                requestorTeam:"reqteam moto",
                requestorDepartment:"reqdept moto",
                requestDate:"20190105",
                supervisor: true,
                containerCount: 2,
                attributes: "attribute moto 1, attribute moto 2",
                remarks: "remarks moto"
            }
        }
        let wrapper = shallow(<RequestInfoCard {...props}/>)
        expect(wrapper).toHaveComponent(Card)
        const textAreas = findManyComponents(wrapper, TextArea)
        expect(wrapper.render().text()).toContain("Awardee: awardee moto")
        expect(wrapper.render().text()).toContain("Team: team moto")
        expect(wrapper.render().text()).toContain("Cluster: dept moto")
        expect(wrapper.render().text()).toContain("Requestor: requestor moto")
        expect(wrapper.render().text()).toContain("Team: reqteam moto")
        expect(wrapper.render().text()).toContain("Cluster: reqdept moto")
        expect(wrapper.render().text()).toContain("Awardee Supervisor: Yes")
        expect(wrapper.render().text()).toContain("Request Date: 01/05/2019")
        expect(wrapper.render().text()).toContain("Attributes:")
        expect(textAreas.at(0).props().value).toEqual("attribute moto 1, attribute moto 2")
        expect(wrapper.render().text()).toContain("Remarks:")
        expect(textAreas.at(1).props().value).toEqual("remarks moto")
    })
})