import {shallow} from 'enzyme'
import React from "react"
import {SideBar} from "../../../main/js/components/SideBar"
import {Menu} from "semantic-ui-react"

describe('SideBar  ', () => {
    it('renders the component', () => {
        const wrapper = shallow(<SideBar/>)
        expect(wrapper).toHaveComponent(Menu)
    })

})