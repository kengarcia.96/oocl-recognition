package com.community.recognition.award;

public class AwardEntityBuilder {

  AwardEntity entity;

  public AwardEntityBuilder() {
    this.entity = new AwardEntity();
  }

  public AwardEntityBuilder setId(Long id) {
    this.entity.setId(id);
    return this;
  }

  public AwardEntityBuilder setAwardeeUserId(Long awardeeUserId) {
    this.entity.setAwardeeUserId(awardeeUserId);
    return this;
  }

  public AwardEntityBuilder setRequestorUserId(Long requestorUserId) {
    this.entity.setRequestorUserId(requestorUserId);
    return this;
  }

  public AwardEntityBuilder setSupervisor(boolean isSupervisor) {
    this.entity.setSupervisor(isSupervisor);
    return this;
  }

  public AwardEntityBuilder setAttributes(String attributes) {
    this.entity.setAttributes(attributes);
    return this;
  }

  public AwardEntityBuilder setRemarks(String remarks) {
    this.entity.setRemarks(remarks);
    return this;
  }

  public AwardEntityBuilder setApprovalStatus(String approvalStatus) {
    this.entity.setApprovalStatus(approvalStatus);
    return this;
  }

  public AwardEntityBuilder setApprovalDate(String approvalDate) {
    this.entity.setApprovalDate(approvalDate);
    return this;
  }

  public AwardEntityBuilder setRejectionRemarks(String rejectionRemarks) {
    this.entity.setRejectionRemarks(rejectionRemarks);
    return this;
  }

  public AwardEntityBuilder setAwardeeTeam(String awardeeTeam) {
    this.entity.setAwardeeTeam(awardeeTeam);
    return this;
  }

  public AwardEntityBuilder setAwardeeCluster(String awardeeCluster) {
    this.entity.setAwardeeCluster(awardeeCluster);
    return this;
  }

  public AwardEntityBuilder setRequestorCluster(String requestorCluster) {
    this.entity.setRequestorCluster(requestorCluster);
    return this;
  }

  public AwardEntityBuilder setRequestorTeam(String requestorTeam) {
    this.entity.setRequestorTeam(requestorTeam);
    return this;
  }

  public AwardEntityBuilder setPoints(int points) {
    this.entity.setPoints(points);
    return this;
  }

  public AwardEntityBuilder setRequestedDate(String requestedDate) {
    this.entity.setRequestedDate(requestedDate);
    return this;
  }

  public AwardEntity build() {
    return this.entity;
  }

  public AwardEntity initialData() {
    setId(1L);
    setAwardeeUserId(1L);
    setRequestorUserId(12L);
    setSupervisor(true);
    setAttributes("Sample Attributes");
    setRemarks("Sample Remarks");
    setApprovalStatus("PENDING");
    setApprovalDate("");
    setRejectionRemarks("");
    setPoints(1);
    setAwardeeTeam("SHP");
    setAwardeeCluster("SHIPMENT SERVICES");
    setRequestorTeam("SHP");
    setRequestorCluster("SHIPMENT SERVICES");
    setRequestedDate("20190802");

    return this.entity;
  }

}
