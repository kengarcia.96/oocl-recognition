package com.community.recognition.milestone;

import com.community.recognition.user.UserEntity;
import com.community.recognition.user.UserEntityBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class MilestoneControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();
    private MilestoneController milestoneController;

    @Mock
    private MilestoneService milestoneService;

    @Before
    public void setUp() {
        milestoneController = new MilestoneController(milestoneService);
        mockMvc = MockMvcBuilders.standaloneSetup(milestoneController).build();
    }

    @Test
    public void getAll() throws Exception {
        UserEntity userEntity = new UserEntityBuilder().initialData();
        MilestoneEntity milestoneEntity = new MilestoneEntityBuilder().initialData(userEntity);
        when(milestoneService.getAll()).thenReturn(Collections.singletonList(milestoneEntity));
        mockMvc.perform(get("/api/milestone/getAll")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.singletonList(milestoneEntity))));
    }
}