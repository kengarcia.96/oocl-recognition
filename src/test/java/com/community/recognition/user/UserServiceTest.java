package com.community.recognition.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        userService = new UserService(userRepository, passwordEncoder);
    }

    @Test
    public void getAllUsersTest() {
        List<UserEntity> expectedUserEntities = new ArrayList<>();
        UserEntity userEntity = new UserEntity(0L, "dopey09", "doofus", "USER", "Dobby",
                "The", "Elf", "Hogwarts", "Dobby10@gmail.com", "Harry");
        expectedUserEntities.add(userEntity);

        when(userRepository.findAll()).thenReturn(expectedUserEntities);

        Iterable<UserEntity> userEntities = userService.getAll();
        assertEquals(1, ((Collection<?>) userEntities).size());
        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void getUserByIdTest() {
        Optional<UserEntity> expectedEntity = Optional.of(new UserEntity(0L, "dopey09", "doofus", "USER", "Dobby",
                "The", "Elf", "Hogwarts", "Dobby10@gmail.com", "Harry"));

        when(userRepository.findById(0L)).thenReturn(expectedEntity);

        Optional<UserEntity> userEntity = userService.getUserById(0L);

        assertEquals("dopey09", userEntity.get().getUsername());
        assertEquals("Dobby", userEntity.get().getFirstname());

    }

    @Test
    public void createUserTest() {
        UserEntity newEntity = new UserEntity(0L, "dopey09", "doofus", "USER", "Dobby",
                "The", "Elf", "Hogwarts", "Dobby10@gmail.com", "Harry");

        newEntity.setPassword(passwordEncoder.encode(newEntity.getPassword()));
        when(userRepository.save(newEntity)).thenReturn(newEntity);
        UserEntity userEntity = userService.addUser(newEntity);

        assertEquals("dopey09", userEntity.getUsername());
        assertEquals("Dobby", userEntity.getFirstname());
        verify(userRepository, times(1)).save(newEntity);
    }

    @Test
    public void updateUserTest() throws Exception {

        when(userRepository.existsById(0L)).thenReturn(true);

        UserEntity updatedEntity = new UserEntity(0L, "dopey09", "doofus", "USER", "Kreacher",
                "The", "Elf", "Hogwarts", "Dobby10@gmail.com", "Harry");

        when(userRepository.save(updatedEntity)).thenReturn(updatedEntity);

        UserEntity userEntity = userService.updateUser(updatedEntity, 0L);

        assertEquals("dopey09", userEntity.getUsername());
        assertEquals("Kreacher", userEntity.getFirstname());
        verify(userRepository, times(1)).save(updatedEntity);
    }

    @Test
    public void deleteUserTest() throws Exception {
        when(userRepository.existsById(0L)).thenReturn(true);
        userService.deleteUser(0L);
        verify(userRepository, times(1)).deleteById(0L);
    }

    @Test
    public void getUserByUsername() {
        UserEntity expectedEntity = new UserEntity(0L, "dopey09", "doofus", "USER", "Dobby",
                "The", "Elf", "Hogwarts", "Dobby10@gmail.com", "Harry");

        when(userRepository.findByUsername("dopey09")).thenReturn(expectedEntity);

        UserEntity userEntity = userService.getUserByUsername("dopey09");

        assertEquals("dopey09", userEntity.getUsername());
        assertEquals("Dobby", userEntity.getFirstname());
        assertEquals(userEntity, expectedEntity);
    }
}


