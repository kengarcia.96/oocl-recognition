package com.community.recognition.user;

public class UserEntityBuilder {

  UserEntity entity;

  public UserEntityBuilder() {
    this.entity = new UserEntity();
  }

  public UserEntityBuilder setId(Long id) {
    this.entity.setId(id);
    return this;
  }

  public UserEntityBuilder setUsername(String username) {
    this.entity.setUsername(username);
    return this;
  }

  public UserEntityBuilder setPassword(String password) {
    this.entity.setPassword(password);
    return this;
  }

  public UserEntityBuilder setRole(String role) {
    this.entity.setRole(role);
    return this;
  }

  public UserEntityBuilder setFirstname(String firstname) {
    this.entity.setFirstname(firstname);
    return this;
  }

  public UserEntityBuilder setMiddlename(String middlename) {
    this.entity.setMiddlename(middlename);
    return this;
  }

  public UserEntityBuilder setLastname(String lastname) {
    this.entity.setLastname(lastname);
    return this;
  }

  public UserEntityBuilder setTeam(String team) {
    this.entity.setTeam(team);
    return this;
  }

  public UserEntityBuilder setCluster(String cluster) {
    this.entity.setCluster(cluster);
    return this;
  }

  public UserEntityBuilder setEmail(String email) {
    this.entity.setEmail(email);
    return this;
  }

  public UserEntity build() {
    return this.entity;
  }

  public UserEntity initialData() {
    setId(1L);
    setUsername("doejo");
    setPassword("doejo");
    setRole("Programmer");
    setFirstname("John");
    setMiddlename("Cruz");
    setLastname("Doe");
    setEmail("john.doe@oocl.com");
    setTeam("SHP");
    setCluster("SHIPMENT SERVICES");
    return this.entity;
  }

}
